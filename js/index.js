let alumnos = [];

const inputNombres = document.getElementById('nombres');
const inputApellidos = document.getElementById('apellidos');
const inputCurso = document.getElementById('curso');

const obtenerDatosFormulario = () => {
  return {
    nombres: inputNombres.value,
    apellidos: inputApellidos.value,
    curso: inputCurso.value,
  };
};

const actualizarTabla = () => {
  const tbody = document.querySelector('tbody');
  tbody.innerHTML = '';

  let i = 0;
  let contenido = '';

  alumnos.forEach((alumno) => {
    i++;
    contenido += `
      <tr>
        <th scope="row">${i}</th>
        <td>${alumno.nombres}</td>
        <td>${alumno.apellidos}</td>
        <td>${alumno.curso}</td>
      </tr>
    `;
  });

  tbody.innerHTML = contenido;
};

const guardar = () => {
  alumnos.push(obtenerDatosFormulario());
  actualizarTabla();
};

const borrar = () => {
  inputNombres.value = '';
  inputApellidos.value = '';
  inputCurso.value = '';
};

const borrarLista = () => {
  alumnos = [];
  const tbody = document.querySelector('tbody');
  tbody.innerHTML = `
    <tr>
      <td colspan="4">Lista vacia, ningun alumno registrado...</td>
    </tr>
  `;
};

const formulario = document.querySelector('form');
formulario.onsubmit = (e) => {
  e.preventDefault();
  guardar();
};

const btnCancelar = document.getElementById('btn-cancelar');
btnCancelar.onclick = borrar;

const btnBorrarLista = document.getElementById('btn-borrar-lista');
btnBorrarLista.onclick = borrarLista;
